#!/usr/bin/env sh

pkgdir="$1"
pkgname="$2"

install -Dm644 README.md "$pkgdir/usr/share/doc/${pkgname}/README.md"

install -Dm755 main.sh "$pkgdir/usr/bin/rpi-coreos-installer"
